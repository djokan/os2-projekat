extern "C" {
	#include "slab.h"

}
#include <iostream>
#include <mutex>
#include "utilities.h"
#include "blockheader.h"
#include "cache.h"
#include "slabElem.h" 
#include "sizencaches.h"
using namespace std;

void strcpy(char* a, const char* b, int n)
{
	int i;
	for ( i = 0; i < n && b[i]!='\0'; i++)
		a[i] = b[i];
	if (i < n && b[i] == '\0')
	{
		a[i] = b[i];
	}
}
// Testirano radi

int strcmp( const char* a,const char* b, int n)
{
	int t = 1;
	for (int i = 0; i < n && b[i] != '\0'; i++)
		if (a[i] != b[i])
		{
			t = 0;
			break;
		}
	return t;
}

/*int getBuddyTreeSize(int blockNum)
{
	int i = 1;
	int sum = 0;
	for (i = 1; i < blockNum; i *= 2)
		sum += i;
	sum += i;
	return sum / 8 + +(((sum % 8) == 0) ? 0 : 1);
}*/

int getBuddyAllocSize(int blockNum)
{
	return blockNum / 8 + (((blockNum % 8) == 0) ? 0 : 1);
}
// Testirano radi

int getBuddyHeaderSize(int blockNum)
{
	return getBuddyAllocSize(blockNum);
}
// Testirano radi

void deallocBlocks(void* space, int blockNum ,int size)
{
	for (int i = 0; i < size; i++)
	{
		setBit(space, blockNum + i, 0);
	}
}

int allocBlocks(void* space, int blockNum, int num)
{
	int t = 0;
	int  trenbl = 0;
	if (num & (num - 1))
	{
		return -1;
	}
	while (trenbl<blockNum)
	{
		t = 0;
		while (t < num && getBit(space, trenbl+t)==0)
		{
			t++;
		}
		if (t == num)
		{
			t = 0;
			while (t < num)
			{
				setBit(space, trenbl+t, 1);
				t++;
			}
			return trenbl;
		}
		trenbl += num;
	}
	if (trenbl >= blockNum)
	{
		return -2;
	}

}
// Testirano radi

void setErrorMessage(Cache* c, char* a)
{
	strcpy((char*)c->error, a, ERROR_SIZE);
	*((char*)c->error + ERROR_SIZE-1) = '\0';
}

int getBit(void* adr, int num)
{
	Byte* a= (Byte*)adr + num / 8;
	Byte b = 0x80;
	for (int i = 0; i < num % 8; i++)
		b = b >> 1;
	b = b & (*a);
	if (b) return 1;
	return 0;
}
// Testirano radi

void setBit(void* adr, int num, int val)
{
	Byte* a = (Byte*)adr + num / 8;
	Byte b = 0x80;
	for (int i = 0; i < num % 8; i++)
		b = b >> 1;
	if (val == 0)
	{
		b = 0xFF - b;
		*a = *a & b;
	}
	else
	{
		*a = *a | b;
	}
}
// Testirano radi
int howMuchBlocks(int byteNum)
{
	int i = 1;
	while (i*BLOCK_SIZE<byteNum)
	{
		i *= 2;
	};
	return i;
}

void* add(const void*a, int b)
{
	return (Byte*)a + b;
}
// Testirano radi

void initBuddy(void* space, int blockNum)
{
	if (afterError(blockNum)+MIN_TRANSLATOR_SIZE > BLOCK_SIZE*blockNum)
	{
		std::cout << "Nema dovoljno prostora!";
		system("pause");
		exit(1);
	}
	unsigned char *adr= (unsigned char*)space ;
	for (int i = 0; i < getBuddyHeaderSize(blockNum); i++)
	{
		*adr = 0;
		adr++;
	}
	allocBlocks(space, blockNum, howMuchBlocks(afterError(blockNum) + MIN_TRANSLATOR_SIZE));
	sizenCaches* siz = (sizenCaches*)add(space, getBuddyHeaderSize(blockNum));
	for (int i = 0; i < 13; i++)
		siz->sizen[i] = NULL;
}

void ispisi(void* adr, int num)
{
	for (int i = 0; i < num; i++)
	{
		if (i % 10 == 0 && i != 0) printf(" ");
		if (i % 100 == 0 && i != 0) printf("\n");
		printf("%d", getBit(adr, i));
	}
}

int afterError(int a)
{
	return  getBuddyHeaderSize(a) + sizeof(sizenCaches) + sizeof(mutex);
}

void initTranslatorBlock(void* space, int blockNum)
{
	BlockHeader* b = (BlockHeader*)(add(space,afterError(blockNum)));
	b->next = b;
	b->prev = b;
	b->number = 0;
	b->free = add(b, sizeof(BlockHeader));
	initFreeBlock(space, howMuchBlocks(afterError(blockNum) + MIN_TRANSLATOR_SIZE), add(b, sizeof(BlockHeader)),sizeof(void*),&b->free);
}
// Testirano radi

void initFreeBlock(void* space, int numOfBlocks, void* start, int size, void* free)
{
	void *t = start;
	int i=0;
	while ( add(start,size) < getBlockAddr(space, numOfBlocks ))
	{
		i++;
		pointTo(start, add(start, size));
		start = add(start, size);
	}
	pointTo(add(start, -size), free);
}
// Testirano radi


void initObjectBlock(void* space, int numOfBlocks, void* start, int size, void* func, void* free)
{
	void *t = start;
	while (add(start, size) < getBlockAddr(space, numOfBlocks ))
	{
		if (func!=NULL)
			((void(*)(void*))func)(add(start,sizeof(void*)));
		pointTo(start, add(start, size));
		start = add(start, size);
	}
	pointTo(add(start, -size), free);
}


void initCacheBlock(void* space, int blockNum)
{
	int cur = allocBlocks(space, blockNum,1);
	void * adr = getBlockAddr(space, cur);
	BlockHeader* b = (BlockHeader*)adr;
	b->next = b;
	b->prev = b;
	b->number = 0;
	b->free = add(b, sizeof(BlockHeader));
	initFreeBlock(adr, 1, add(adr, sizeof(BlockHeader)), sizeof(Cache), &b->free);
}

void initStaticHeader(void * space, int blockNum)
{
	pointTo((Byte*)space + afterError(blockNum), (Byte*)space + afterStaticHeader(blockNum));
	pointTo((Byte*)space + afterError(blockNum) + sizeof(void*), getBlockAddr(space,howMuchBlocks(afterError(blockNum) + MIN_TRANSLATOR_SIZE)));
}

int afterStaticHeader(int blockNum)
{
	return afterError(blockNum)+STATIC_HEADER_SIZE;
}

void initSlabBlock(void* space, int blockNum)
{
	int cur = allocBlocks(space, blockNum, 1);
	void * adr = getBlockAddr(space, cur);
	BlockHeader* b = (BlockHeader*)adr;
	b->next = b;
	b->prev = b;
	b->number = 0;
	b->free = add(b, sizeof(BlockHeader));
	initFreeBlock(adr, 1, add(adr, sizeof(BlockHeader)), sizeof(Slab), &b->free);
}

void pointTo(void* a, void* b)
{
	Translator* t = (Translator*)((Byte*)a );
	t->val = b;
}

void* getPointer(void* a)
{
	Translator* t = (Translator*)((Byte*)a);
	return t->val;
}

void* getBlockAddr(void* space,int a)
{
	return add(space,a*BLOCK_SIZE);
}

BlockHeader* getCacheHeader(void* space, int blockNum)
{
	return (BlockHeader*)getBlockAddr(space, howMuchBlocks(afterError(blockNum) + MIN_TRANSLATOR_SIZE));
}

BlockHeader* getTranslatorHeader(void* space, int blockNum)
{
	return (BlockHeader*)(add(space, afterError(blockNum)));
}

BlockHeader* getSlabHeader(void* space, int blockNum)
{
	return (BlockHeader*)getBlockAddr(space, howMuchBlocks(afterError(blockNum) + MIN_TRANSLATOR_SIZE)+1);
}

int calcSlabSize(int numBlock,size_t objSize)
{
	objSize += sizeof(void*);
	int init = howMuchBlocks(objSize);
	int best = 1;
	float percent = 0;
	float maxpercent = 0;
	int t = init;
	while (t < init*16 && numBlock*BLOCK_SIZE/t>10)
	{
		percent = 1 - ((float)((BLOCK_SIZE*t - 8) % objSize)) / (BLOCK_SIZE*t - 8);
		//cout << objSize << " " << BLOCK_SIZE*t-8 << " " << percent << endl;
		if (percent > 0.9)
		{
			best = t;
			break;
		}
		if (percent > maxpercent)
		{
			maxpercent = percent;
			best = t;
		}
		t *= 2;
	}
	return best;
}

void* newTranslator(void* space, int blockNum, void* aa)
{
	BlockHeader* header= getTranslatorHeader(space, blockNum)->prev;
	void *tmp;
	tmp = header->free;
	if (tmp == &header->free)
	{
		header = initNewBlock(space, blockNum, getTranslatorHeader(space, blockNum), 1, sizeof(void*),(Cache*)aa);
		tmp = header + 1;
	}
	header->number++;
	header->free = getPointer(header->free);
	pointTo(tmp, aa);
	return tmp;
}

void deleteTranslator(void* space, int blockNum, void* a)
{
	BlockHeader* header = getTranslatorHeader(space, blockNum);
	if (a > getBlockAddr(space, howMuchBlocks(afterError(blockNum) + MIN_TRANSLATOR_SIZE)))
	{
		header = header->next;
		while (a >= getBlockAddr(header, 1) || a <= getBlockAddr(header, 0)  )
		{
			header = header->next;
		}
	}
	header->number--;
	pointTo(a, header->free);
	header->free= a;
	if (header->number == 0 && (((Byte*) header)- (Byte*)space>BLOCK_SIZE*howMuchBlocks(blockNum)))
	{
		deleteBlock(space, header,1);
	}
}

int getBlockNum(void* space, void* startAddr)
{
	return (((Byte*)startAddr) - (Byte*)space) / BLOCK_SIZE;
}

void deleteBlock(void* space, BlockHeader* b,int size)
{
	b->prev->next = b->next;
	b->next->prev = b->prev;
	int number = getBlockNum(space,b);
	deallocBlocks(space, number, size);
}



BlockHeader* initNewBlock(void* space, int blockNum, BlockHeader* initblck, int sizeofBlock, int sizeofObj, Cache* c)
{
	int cur = allocBlocks(space, blockNum, sizeofBlock);

	if (cur == -1)
	{
		setErrorMessage(c, "Velicina koja se zauzima nije stepen dvojke!");
	}
	if (cur == -2)
	{
		setErrorMessage(c, "Nema dovoljno prostora za zauzimanje novog bloka alokatora!");
	}
	void * adr = getBlockAddr(space, cur);
	BlockHeader* b = (BlockHeader*)adr;
	b->next = b;
	b->prev = b;
	b->number = 0;
	b->free = add(b, sizeof(BlockHeader));
	initFreeBlock(adr, sizeofBlock, add(adr, sizeof(BlockHeader)), sizeofObj, &b->free);
	b->prev = initblck->prev;
	b->next = initblck;
	initblck->prev->next = b;
	initblck->prev = b;
	return b;
}


void test(void* space, int blockNum, void* d)
{
	Slab* s = (Slab*)d;
	void* temp = s->slabBlocks->free;
	void* free = &s->slabBlocks->free;

	int i = 0;
	while (temp != free)
	{
		i++;
		temp = getPointer(temp);
	}
	//cout << i << endl;
}

void slabsInfo(Slab* f)
{
	if (f == NULL)
	{
		cout << "Nema" << endl;
		return;
	}
	Slab* t = f;
	int i = 1;
	do
	{
		cout << i << ". " << endl;
		slabInfo(f);
		i++;
		f = f->next;
	} while (f!=t);
}

void slabInfo(Slab* f)
{
	cout << "Adresa pocetnog bloka:" << f->slabBlocks << endl;
	cout << "Broj objekata unutar ploce:" <<  f->number << endl;
}


void cachecpy(Cache* c, Cache* c1)
{
	c->con = c1->con;
	c->des = c1->des;
	strcpy(c->name, c1->name, NAME_LEN);
	strcpy(c->error, c1->error, ERROR_SIZE);
	c->empty = c1->empty;
	c->full = c1->full;
	c->partial = c1->partial;
	c->slabSize = c1->slabSize;
	c->tra = c1->tra;
	c->objSize = c1->objSize;
}

Slab* newSlab(void* space, int blockNum, int slabSize, int objSize, void* con,Cache* parent)
{
	parent->uslov = 1;
	BlockHeader* header = getSlabHeader(space, blockNum)->prev;
	Slab* newSlab;
	if (header->free == &header->free)
	{
		header = initNewBlock(space, blockNum, getSlabHeader(space, blockNum), 1, sizeof(Slab), parent);
		newSlab = (Slab*)(header + 1);
		header->free = getPointer((header->free));
	}
	else if (header->number==0)
	{
		newSlab = (Slab*)(header + 1);
		header->free = getPointer((header->free));
	}
	else
	{
		newSlab = (Slab*)header->free;
		header->free = getPointer((header->free));
	}
	header->number++;
	
	
	newSlab->parent = parent;
	newSlab->number = 0;
	int offset = parent->offset;
	parent->offset += CACHE_L1_LINE_SIZE;
	if (parent->offset > (((int)BLOCK_SIZE)*parent->slabSize - 8) % (objSize + ((int)sizeof(void*))))
	{
		parent->offset = 0;
	}
	newSlab->slabBlocks = newObjBlock(space, blockNum, slabSize, objSize, con, offset,parent);
	return newSlab;
}

ObjBlockHeader* newObjBlock(void* space, int  blockNum, int slabSize, int objSize, void* con, int offset, Cache* c)
{
	int i = allocBlocks(space, blockNum, slabSize);
	
	if (i == -1)
	{
		setErrorMessage(c, "Velicina koja se zauzima nije stepen dvojke!");
		return NULL;
	}
	if (i == -2)
	{
		setErrorMessage(c, "Nema dovoljno prostora za zauzimanje novog bloka alokatora!");
		return NULL;
	}
	
	ObjBlockHeader* header = (ObjBlockHeader*) getBlockAddr(space, i);
	header->free = add(header + 1, offset);
	initObjectBlock(header, slabSize, add(header + 1, offset), objSize+sizeof(void*), con, &(header->free));
	return header;
}

void deleteSlab(void* space, int blockNum, Slab* slab)
{

	BlockHeader* h = getSlabHeader(space, blockNum);

	while ((slab < (void*)h) || slab > getBlockAddr(h, 1))
	{
		h = h->next;
	}

	Slab* s1 = ((Slab*)(getSlabHeader(space, blockNum)->prev + 1))->prev;

	BlockHeader* h2 = getSlabHeader(space, blockNum)->prev;

	h2->number--;
	int numberOfBlock = getBlockNum(space, slab->slabBlocks);
	deallocBlocks(space, numberOfBlock, slab->parent->slabSize);
	pointTo(s1, h2->free);
	slabcpy(slab, s1);
	h2->free = s1;
	
	if (h2->number == 0 && h2 != getSlabHeader(space, blockNum))
	{
		deleteBlock(space, h2, 1);
	}

}

void moveToSlabList(Slab** src, Slab** dst)
{
	if ((*dst) == NULL)
	{
		*dst = *src;
		if ((*src) == (*src)->next)
		{
			(*src) = NULL;
		}
		else
		{
			(*src)->next->prev = (*src)->prev;
			(*src)->prev->next = (*src)->next;
			(*src) = (*src)->next;
		}
		(*dst)->next = (*dst);
		(*dst)->prev = (*dst);
	}
	else
	{
		Slab* t = (*src);
		if ((*src) == (*src)->next)
		{
			(*src) = NULL;
		}
		else
		{
			(*src)->next->prev = (*src)->prev;
			(*src)->prev->next = (*src)->next;
			(*src) = (*src)->next;
		}
		t->next = (*dst)->next;
		t->prev = (*dst);
		(*dst)->next->prev = t;
		(*dst)->next = t;
	}
}

void slabcpy(Slab* s1, Slab* s2)
{
	s1->prev = s2->prev;
	s1->next = s2->next;
	s1->number = s2->number;
	s1->parent = s2->parent;
	s1->slabBlocks = s2->slabBlocks;
}

void ejectFromCache(Slab* s)
{
	s->prev->next = s->next;
	s->next->prev = s->next;
	if (s->parent->empty == s)
	{
		s->parent->empty = s->next;
		if (s->parent->empty == s)
		{
			s->parent->empty = NULL;
		}
	}

	if (s->parent->partial == s)
	{
		s->parent->partial = s->next;
		if (s->parent->partial == s)
		{
			s->parent->partial = NULL;
		}
	}

	if (s->parent->full == s)
	{
		s->parent->full = s->next;
		if (s->parent->full == s)
		{
			s->parent->full = NULL;
		}
	}
}

void insertToCache(Slab** dst,Slab* s)
{
	if (*dst == NULL)
	{
		*dst = s;
		s->next = s;
		s->prev = s;
	}
	else
	{
		s->prev = (*dst);
		s->next = (*dst)->next;
		(*dst)->next->prev = s;
		(*dst)->next = s;
	}
}

