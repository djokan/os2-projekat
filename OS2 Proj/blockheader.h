#pragma once

struct BlockHeader
{
	int number;
	BlockHeader * prev;
	BlockHeader * next;
	void * free;
};

struct ObjBlockHeader
{
	void* free;
};