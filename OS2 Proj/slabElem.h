#pragma once

struct Cache;

struct Slab {
	Slab* prev;
	Slab* next;
	int number;
	ObjBlockHeader* slabBlocks;
	Cache* parent;
};