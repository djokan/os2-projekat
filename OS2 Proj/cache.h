#pragma once

#include "consts.h"

#define NAME_LEN 25

struct Slab;

struct Cache {
	char name[NAME_LEN];
	char error[ERROR_SIZE];
	Cache* next;
	Cache* prev;
	Slab* full;
	Slab* empty;
	Slab* partial;
	void* con;
	int uslov;
	int objSize;
	int offset;
	void* des;
	void* tra;
	int slabSize;
};
