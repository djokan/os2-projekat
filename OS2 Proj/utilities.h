#pragma once

#include "blockheader.h"

#include "cache.h" 


#include "consts.h"

void* add(const void*, int);

void strcpy(char*,const char*,const int);

int calcSlabSize(int,size_t);

int strcmp(const char*,const char*, int);

int getBuddyHeaderSize(int);

int getBuddyTreeSize(int);

int getBuddyAllocSize(int);

void initBuddy(void* , int);

int allocBlocks(void* , int , int );

int getBit(void*, int);

void setBit(void*, int, int);

void setErrorMessage(Cache* , char*);

void ispisi(void* , int );

int afterError(int);

void initTranslatorBlock(void*, int);

void initCacheBlock(void*, int);

void initStaticHeader(void*, int);

int afterStaticHeader(int);

void initSlabBlock(void*, int);

int howMuchBlocks(int byteNum);

struct Translator {
	void* val;
};

void pointTo(void*, void*);

void* getPointer(void*);

void* getBlockAddr(void* , int );

void initObjectBlock(void*, int, void*, int, void*);

void initFreeBlock(void* , int , void* , int , void*);

BlockHeader* getCacheHeader(void* , int);

BlockHeader* getTranslatorHeader(void*, int);

BlockHeader* getSlabHeader(void*, int);

void* newTranslator(void* , int , void* );

void test(void*, int, void*);

BlockHeader* initNewBlock(void* space, int blockNum, BlockHeader* initblck, int sizeofBlock, int sizeofObj, Cache*);

void deleteBlock(void* space, BlockHeader* b, int size);

void deleteTranslator(void* space, int blockNum, void* a);

void cachecpy(Cache* c, Cache* c1);

Slab* newSlab(void* space, int blockNum, int slabSize, int objSize, void* con, Cache* parent);

void slabcpy(Slab*, Slab*);

ObjBlockHeader* newObjBlock(void* space, int  blockNum, int slabSize, int objSize, void* con, int offset,Cache*);

void deleteSlab(void* space, int blockNum, Slab* slab);


void moveToSlabList(Slab** src, Slab** dst);

void ejectFromCache(Slab* s);

void insertToCache(Slab** dst, Slab* s);

void slabsInfo(Slab* f);

void slabInfo(Slab* f);
