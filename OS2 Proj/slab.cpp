#include "cache.h"
#include "utilities.h"
#include <iostream>
#include "slabElem.h"
#include "sizencaches.h" 
#include <string> 
#include <mutex>

extern "C" {
	#include "slab.h"
}
using namespace std;
void * space;
int blockNum;

extern "C" {
	mutex* getMutex(void* space, int blockNum)
	{
		mutex* s = (mutex*)add(space, getBuddyHeaderSize(blockNum) + sizeof(sizenCaches));
		return s;
	}
}
extern "C" void kmem_init(void *space1, int block_num)
{
	if (BLOCK_SIZE < 60)
	{
		printf("Velicina bloka je premala!\n");
		system("pause");
		exit(1);
	}
	space = space1;
	blockNum = block_num;
	initBuddy(space, blockNum);
	initTranslatorBlock(space, blockNum);
	initCacheBlock(space, blockNum);
	initSlabBlock(space, blockNum);
	mutex* m = getMutex(space, blockNum);
	new (m) mutex();
}

extern "C" kmem_cache_t *kmem_cache_create(const char *name, size_t size,
	void(*ctor)(void *),
	void(*dtor)(void *))
{
	if (strcmp(name,"size-", 6)==0)	getMutex(space,blockNum)->lock();
	Cache* newCache;
	BlockHeader* header = getCacheHeader(space, blockNum)->prev;
	if (header->free == &(header->free))
	{
		header = initNewBlock(space, blockNum, getCacheHeader(space, blockNum), 1, sizeof(Cache),NULL);
		newCache = (Cache*)(header + 1);
		newCache->next = newCache;
		newCache->prev = newCache;
	}
	else
	if (header->number==0)
	{
		newCache = (Cache*)(header + 1);
		newCache->next = newCache;
		newCache->prev = newCache;
	}
	else
	{
		newCache = (Cache*)header->free;
		Cache* first = (Cache*)(header + 1);
		newCache->next = first;
		newCache->prev = first->prev;
		first->prev->next = newCache;
		first->prev = newCache;
	}
	header->free = getPointer(header->free);
	header->number++;
	strcpy((char*)(newCache->name), name, NAME_LEN);
	newCache->empty = NULL;
	newCache->full= NULL;
	newCache->partial = NULL;
	newCache->con = ctor;
	setErrorMessage(newCache, "Nema gresaka!");
	newCache->des = dtor;
	newCache->offset = 0;
	newCache->objSize = size;
	newCache->slabSize = calcSlabSize(blockNum,size);
	newCache->tra = newTranslator(space, blockNum, newCache);
	if (strcmp(name, "size-", 6) == 0)	getMutex(space,blockNum)->unlock();
	return (kmem_cache_t *) newCache->tra;
} // Allocate cache
extern "C" int kmem_cache_shrink(kmem_cache_t *cachep)
{
	getMutex(space,blockNum)->lock();
	Cache* c = (Cache*)getPointer(cachep);
	setErrorMessage(c, "Nema gresaka!");
	if (c->uslov == 1 && c->empty!=NULL)
	{
		Slab*s = c->empty;
		s->prev->next = NULL;
		c->empty = NULL;
		Slab* t;
		while (s!=NULL)
		{
			t = s;
			s = s->next;
			deleteSlab(space, blockNum, t);
		}
	}
	c->uslov = 0;
	//test(space, blockNum);
	getMutex(space,blockNum)->unlock();
	return 0;
} // Shrink cache
extern "C" void *kmem_cache_alloc(kmem_cache_t *cachep)
{
	if (strcmp(((Cache*)getPointer(cachep))->name, "size-", 6) == 0) getMutex(space,blockNum)->lock();
	Cache* c = (Cache*)getPointer(cachep);
	setErrorMessage(c, "Nema gresaka!");
	void* obj=NULL;
	if (c->empty == NULL && c->partial == NULL)
	{
		c->partial = newSlab(space, blockNum, c->slabSize, c->objSize, c->con, c);
		if (c->partial->slabBlocks == NULL)
		{
			if (strcmp(((Cache*)getPointer(cachep))->name, "size-", 6) == 0) getMutex(space,blockNum)->unlock();
			return NULL;
		}
		c->partial->next = c->partial;
		c->partial->prev = c->partial;
		obj = add(c->partial->slabBlocks->free,sizeof(void*));
		c->partial->slabBlocks->free = getPointer(c->partial->slabBlocks->free);
		c->partial->number++;
		pointTo(add(obj,- (int)sizeof(void*)), c->partial);

	}
	else if (c->partial!=NULL)
	{
		obj = add(c->partial->slabBlocks->free, sizeof(void*));
		c->partial->slabBlocks->free = getPointer(c->partial->slabBlocks->free);
		c->partial->number++;
		pointTo(add(obj, -(int)sizeof(void*)), c->partial);
		if (c->partial->slabBlocks->free == &(c->partial->slabBlocks->free))
		{
			moveToSlabList(&c->partial, &c->full);
		}
	}
	else
	{
		obj = add(c->empty->slabBlocks->free, sizeof(void*));
		c->empty->slabBlocks->free = getPointer(c->empty->slabBlocks->free);
		c->empty->number++;
		pointTo(add(obj, - (int)sizeof(void*)), c->empty);
		moveToSlabList(&c->empty, &c->partial);
	}
	//test(space, blockNum, getPointer(add(obj, -(int)(sizeof(void*)))) );
	if (strcmp(((Cache*)getPointer(cachep))->name, "size-", 6) == 0) getMutex(space,blockNum)->unlock();
	return obj;
} // Allocate one object from cache
extern "C" void kmem_cache_free(kmem_cache_t *cachep, void *objp)
{
	getMutex(space,blockNum)->lock();
	Cache* c = (Cache*)getPointer(cachep);
	setErrorMessage(c, "Nema gresaka!");
	Slab* s = (Slab*)getPointer(add(objp, - (int)sizeof(void*)));
	if (c->des != NULL)
		((void(*)(void*))c->des)(objp);
	if (c->con != NULL)
		((void(*)(void*))c->con)(objp);
	pointTo(add(objp, -(int)sizeof(void*)), s->slabBlocks->free);
	s->slabBlocks->free = add(objp, -(int)sizeof(void*));
	s->number--;
	ejectFromCache(s);
	if (s->number == 0)
	{
		insertToCache(&c->empty, s);
	}
	else
	{
		insertToCache(&c->partial, s);
	}
	//test(space, blockNum, s);

	getMutex(space,blockNum)->unlock();
} // Deallocate one object from cache
extern "C" void *kmalloc(size_t size)
{
	getMutex(space,blockNum)->lock();

	sizenCaches* siz = (sizenCaches*)add(space, getBuddyHeaderSize(blockNum));
	int blockSize = 16;
	int i;
	for ( i = 5; i <= 17; i++)
	{
		blockSize *= 2;
		if (size <= blockSize)
		{
			break;
		}
	}
	if (size > blockSize)
	{
		getMutex(space,blockNum)->unlock();
		return NULL;
	}

	if (siz->sizen[i - 5] == NULL)
	{
		std::string str = std::string("size-")+ std::to_string(i);
		siz->sizen[i - 5] = kmem_cache_create(str.c_str(), blockSize, NULL, NULL);
	}
	void* o=kmem_cache_alloc((kmem_cache_t*)siz->sizen[i - 5]);
	getMutex(space,blockNum)->unlock();

	return o;
} // Alloacate one small memory buffer
extern "C" void kfree(const void *objp)
{
	getMutex(space,blockNum)->lock();

	
	Slab* s = (Slab*)getPointer(add(objp, -(int)sizeof(void*)));
	Cache *c = s->parent;
	pointTo(add(objp, -(int)sizeof(void*)), s->slabBlocks->free);
	s->slabBlocks->free = add(objp, -(int)sizeof(void*));
	s->number--;
	ejectFromCache(s);
	if (s->number == 0)
	{
		insertToCache(&c->empty, s);
	}
	else
	{
		insertToCache(&c->partial, s);
	}

	getMutex(space,blockNum)->unlock();
} // Deallocate one small memory buffer

extern "C" void kmem_cache_destroy(kmem_cache_t *cachep)
{
	getMutex(space,blockNum)->lock();

	Cache* c = (Cache*)getPointer(cachep);
	setErrorMessage(c, "Nema gresaka!");
	BlockHeader* h = getCacheHeader(space, blockNum);

	while ((c < (void*)h) || c > getBlockAddr(h, 1))
	{
		h = h->next;
	}

	Cache* c1 = ((Cache*)(getCacheHeader(space, blockNum)->prev + 1))->prev;

	BlockHeader* h2 = getCacheHeader(space, blockNum)->prev;

	
	pointTo(c1->tra, c);
	cachecpy(c, c1);
	
	c1->prev->next = c1->next;
	c1->next->prev = c1->prev;
	pointTo(c1, h2->free);
	h2->free = c1;
	h2->number--;
	if (h2->number == 0 && h2 != getCacheHeader(space, blockNum))
	{
		deleteBlock(space, h2, 1);
	}
	deleteTranslator(space, blockNum, cachep);

	getMutex(space,blockNum)->unlock();


} // Deallocate cache
extern "C" void kmem_cache_info(kmem_cache_t *cachep)
{
	getMutex(space,blockNum)->lock();

	Cache* c = (Cache*)getPointer(cachep);
	cout << "Ime: " << c->name << endl;
	cout << "Velicina objekta:" << c->objSize << endl; 
	cout << ((c->con != NULL) ? "Ima" : "Nema") << " konstruktor, " << ((c->des != NULL) ? "ima" : "nema") << " destruktor" << endl;
	cout << "Velicina ploce: " << c->slabSize << endl;
	cout << "Prazne ploce: " << endl;
	slabsInfo(c->empty);
	cout << "Delimicno popunjene ploce: " << endl;
	slabsInfo(c->partial); 
	cout << "Potpuno popunjene ploce: " << endl;
	slabsInfo(c->full); 
	getMutex(space,blockNum)->unlock();
} // Print cache info



extern "C" int kmem_cache_error(kmem_cache_t *cachep)
{
	getMutex(space,blockNum)->lock();
	Cache* c = (Cache*)getPointer(cachep);
	printf("%s", c->error);
	int i;
	if (strcmp(c->error, "Nema gresaka!", ERROR_SIZE) == 1)
	{
		i = 0;
	}
	else
	{
		i = 1;
	}

	getMutex(space,blockNum)->unlock();
	return i;
} // Print error message
